# TCD Robot Follower
<p> This repo includes launch files which allow for Turtlebot2 to follow a person in frame using OpenCV and the MobileNetSSD model alongside Tensorflow. When cloning this repository, the following is assumed for the machine: <p/>

* ROS Melodic is installed (dunno if this works for other versions)
* Turtlebot2 packages are installed
* Rospy, RealSense, OpenCV, Tensorflow, and pyttsx3 are installed (requirements.txt will be created in the future).

## Installing package

    mkdir -p ~/<new_repo>/src
    cd <new_repo>
    catkin_make
    cd src
    git clone https://gitlab.com/railundergrads/tcd_robot_follower.git
    cd ..
    catkin_make

## Running launch file
<p> Assuming roscore and turtlebot is running: <p/>

    roslaunch tcd_robot_follower follow_launch.launch
